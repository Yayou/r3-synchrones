node check_sum
  (x: bool)
returns
  (ok: bool);

var
  V28_c: bool;

let
  ok = ((if (not (false -> (pre V28_c))) then (x xor x) else (not (x xor x))) = 
  (false -> (pre x)));
  V28_c = (if (false -> (pre V28_c)) then (x or x) else (x and x));
tel

