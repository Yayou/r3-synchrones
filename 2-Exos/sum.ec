node sum
  (x: bool;
  y: bool)
returns
  (z: bool);

var
  V5_c: bool;

let
  z = (if (not (false -> (pre V5_c))) then (x xor y) else (not (x xor y)));
  V5_c = (if (false -> (pre V5_c)) then (x or y) else (x and y));
tel

