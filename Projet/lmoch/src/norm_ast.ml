open Asttypes

(* Arbres de syntaxe abstraite normalisés *)

type typed_var = Ident.t * base_ty                              
                                               
                             
type n_expr =
    { nexpr_desc: n_expr_desc;
      nexpr_type:  ty;
      nexpr_loc: location;
      nexpr_aux: n_expr option;
    }

 and n_expr_desc =
   | NE_const of const
   | NE_ident of Ident.t
   | NE_cmp of op * n_expr * n_expr
   | NE_comb of op * n_expr list
   | NE_arith of op * n_expr * n_expr
   | NE_if of n_expr * n_expr * n_expr
   | NE_arrow of n_expr * n_expr
   | NE_pre of n_expr
   | NE_tuple of n_expr list

(* pattern : permet d'autoriser (x,y) = expression 
   WARNING : on peut écrire (x,y) = if z then (1,2) else (3,4)
   => il faut reflechir
*)
type n_patt =
    { npatt_desc: Ident.t list;
      npatt_type: ty;
      npatt_loc: location; }

type n_equation =
    { neq_patt: n_patt;
      neq_expr: n_expr; }

type n_node =
    { nn_name: Ident.t;
      nn_input: typed_var list;
      nn_output: typed_var list;
      nn_local: typed_var list;
      nn_equs: n_equation list;
      nn_loc: location; }

type n_file = n_node list
