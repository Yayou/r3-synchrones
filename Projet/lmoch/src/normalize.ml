open Asttypes
open Norm_ast
open Typed_ast

module Env = Map.Make(Ident)


(* let rec print_env = function *)
(*   | [] -> () *)
(*   | (id,ty)::tl -> *)
(*      let p_id = Ident.string_of id in *)
(*      let p_ty = begin *)
(*        match ty with *)
(*          Tbool -> "bool" *)
(*        | Tint -> "int" *)
(*        | Treal -> "real" *)
(*      end in *)
(*      Format.printf "%s %s\n" p_id p_ty ; *)
(*      print_env tl *)

  
(* we create a counter for the auxiliary equations,
   a list of tuples (id, expr) of equations id = expr to be added,
   and a list of tuples (id, typ) of auxiliaries variables to add to locals *)
let cpt_aux = ref 0
let eq_to_add = ref []
let var_to_add = ref []

(* we also create a global environnement to match
   variables with their type.
   We have to do so to create correct patterns when adding equations *)
let var_typ = ref Env.empty

  (* we use a counter for inlining, so that if we call the same node
several times, every ident will have a different name, without depending
     on the id of the variable *)
let cpt_inline = ref 0


let create_patt i =
  (* Format.printf "create_patt %s@." i.Ident.name; *)
  let t = Env.find i !var_typ in
  (* Format.printf *)
  (*   "create_patt %s %s@." *)
  (*   (Ident.string_of i) *)
  (*   (match t with | Tbool -> "bool" | Treal -> "real" | Tint -> "int"); *)
  { npatt_desc = [i] ;
    npatt_type = [t] ;
    npatt_loc = (Lexing.dummy_pos,Lexing.dummy_pos) ;
  }
      
let create_eq i e =
  (* Format.printf "create_eq %s@." (Ident.string_of i); *)
  { neq_patt = create_patt i ;
    neq_expr = e ;
  } 
      
(* let rec add_eqs () = *)
(*   (\* Format.printf "add_eqs\n"; *\) *)
(*   match !eq_to_add with *)
(*   | [] -> [] *)
(*   | (id,expr)::tl -> *)
(*      let eq = create_eq id expr in *)
(*      eq_to_add := tl ; *)
(*      eq :: (add_eqs ()) *)

let type_pat n e =
  List.nth e.texpr_type n
       
let rec break_tuple nt e =
  let new_e =
    match e.nexpr_desc with
    | NE_const _
    | NE_ident _ as e -> e
    | NE_cmp (op, e1, e2) ->
       NE_cmp (op, break_tuple nt e1, break_tuple nt e2)
    | NE_comb (op, el) ->
       NE_comb (op, (List.map (break_tuple nt) el))
    | NE_arith (op, e1, e2) ->
       NE_arith (op, break_tuple nt e1, break_tuple nt e2)
    | NE_if (e1, e2, e3) ->
       NE_if (e1, break_tuple nt e2, break_tuple nt e3)
    | NE_arrow (e1, e2) ->
       NE_arrow (break_tuple nt e1, break_tuple nt e2)
    | NE_pre e ->
       NE_pre (break_tuple nt e)
    | NE_tuple el ->
       (List.nth el nt).nexpr_desc
  in
  { e with nexpr_desc = new_e }

and add_tuple ids e =
  match ids with
  | [] | [_] -> ()
  | hd::tl ->
     eq_to_add := (create_eq hd (break_tuple (List.length tl) e)) :: !eq_to_add;
    add_tuple tl e
  
                  
let get_two_params el =
  match el with
  | [ e1 ; e2 ] -> e1, e2
  | _ -> assert false
                  
let will_be_term = function
  | TE_const _ | TE_ident _ | TE_arrow _ | TE_pre _ | TE_tuple _ -> true
  | TE_op (op,_) ->
     begin
       match op with
       | Op_and | Op_or | Op_impl | Op_not
       | Op_eq | Op_neq | Op_lt | Op_le | Op_gt | Op_ge
         -> false
       | _ -> true
     end
  | _ -> false
     

(* the function inlining takes the list of parameters
   of the function, and the node called,
   and :
   - adds needed equations to eq_to_add
   - returns an ident for the result of the inlined function *)
let rec inlining params node f =
  (* Format.printf "inlining %s@." node.tn_name.Ident.name ; *)
  (* fresh var takes a variable from the function,
     and returns a new variable, with the name of the
    function (for readability) and cpt_inline (for uniqueness) *)
  let fresh_var var =
    let node_name = node.tn_name.Ident.name in
    let var_name =
      Format.sprintf "%s_%s_%i"
        var.Ident.name
        node_name
        !cpt_inline
    in
    Ident.make var_name Ident.Stream
  in
  (* add_fresh_vars takes a list of variable and an environment,
     and adds to the environment a binding between the variables,
     and the associated fresh variable *)
  let add_fresh_vars vl env =
    List.fold_right
      (fun v e ->
        let fv = fresh_var (fst v) in
        let t = snd v in
        var_to_add := (fv, t) :: !var_to_add ;
        var_typ := Env.add fv t !var_typ ;
        Env.add (fst v) fv e)
      vl
      env
  in
  (* replace_vars replace inputs by expressions and other variables
     by fresh variables in the equations *)
  let replace_vars eqs env_vars env_inputs : Typed_ast.t_equation list =
    let replace_patt p =
      let new_desc = List.map (fun n -> Env.find n env_vars) p.tpatt_desc in
      { p with tpatt_desc = new_desc }
    in
    let rec replace_expr e =
      let new_desc =
        match e.texpr_desc with
        | TE_const c -> TE_const c
        | TE_ident i ->
           begin
             try TE_ident (Env.find i env_vars)
             with Not_found -> (Env.find i env_inputs).texpr_desc
           end
        | TE_op (o, el) -> TE_op (o, List.map replace_expr el)
        | TE_app (i, el) -> TE_app (i, List.map replace_expr el)
        | TE_prim _ -> assert false
        | TE_arrow (e1, e2) ->
           TE_arrow (replace_expr e1, replace_expr e2)
        | TE_pre e -> TE_pre (replace_expr e)
        | TE_tuple el -> TE_tuple (List.map replace_expr el)
      in
      { e with texpr_desc = new_desc }
    in
    List.map
      (fun {teq_patt=p;teq_expr=e} ->
        { teq_patt = replace_patt p ; teq_expr = replace_expr e })
      eqs
  in
  let create_res env_vars tv =
    let id = fst tv in
    let ty = snd tv in
    { nexpr_desc = NE_ident (Env.find id env_vars) ;
      nexpr_type = [ty] ;
      nexpr_loc  = Lexing.dummy_pos, Lexing.dummy_pos ;
      nexpr_aux  = None ;
    }
  in
          
  let new_vars = add_fresh_vars (node.tn_local@node.tn_output) Env.empty in
  let new_inputs =
    List.fold_right2
      (fun x y e -> Env.add (fst x) y e)
      node.tn_input
      params
      Env.empty
  in
  (* print_env (Env.bindings !var_typ) ; *)
  let new_eqs : Typed_ast.t_equation list = replace_vars node.tn_equs new_vars new_inputs in
  let norm_eqs : Norm_ast.n_equation list =
    List.map
      (norm_equation f)
      new_eqs
  in
  eq_to_add := norm_eqs @ (!eq_to_add);
  NE_tuple (List.map (create_res new_vars) node.tn_output) 
     
and create_aux ty f (ed:Typed_ast.t_expr_desc) =
  if not(will_be_term ed) then
    let id = Ident.make ("aux"^(string_of_int !cpt_aux)) Ident.Stream in
    incr cpt_aux ;
    let e = { texpr_desc = ed;
              texpr_type = ty;
              texpr_loc = Lexing.dummy_pos, Lexing.dummy_pos }
    in
    let f = norm_expr ~term:false f e in
    let t =
      try List.hd ty
      with _ -> assert false
    in
    var_to_add :=  (id, t) :: !var_to_add ;
    NE_ident id, Some f
  else
    norm_term_desc ty f ed
              
and norm_form_desc ty f e : Norm_ast.n_expr_desc =
  match e with
  | TE_op (op, el) ->
     begin
       match op with
       (* t cmp t *)
       | Op_eq | Op_neq | Op_lt | Op_le ->
          let e1, e2 = get_two_params el in
          NE_cmp
            (op,
             norm_expr ~term:true f e1,
             norm_expr ~term:true f e2)
       | Op_gt ->
          let e1, e2 = get_two_params el in
          NE_cmp
            (Op_lt,
             norm_expr ~term:true f e2,
             norm_expr ~term:true f e1)
       | Op_ge ->
          let e1, e2 = get_two_params el in
          NE_cmp
            (Op_le,
             norm_expr ~term:true f e2,
             norm_expr ~term:true f e1)
       (* f cmb f *)
       | Op_and | Op_or | Op_impl | Op_not ->
          let nel = List.map (norm_expr ~term:false f) el in
          NE_comb (op, nel)
       (* we know it is not a formula, so aux will be None *)
       | _ -> fst(norm_term_desc ty f e)
     end
  | _ -> fst(norm_term_desc ty f e)
    
and norm_term_desc ty f e : Norm_ast.n_expr_desc * Norm_ast.n_expr option =
  match e with
  | TE_const c ->
     NE_const c, None
  | TE_ident i ->
     NE_ident i, None
  | TE_arrow (e1, e2) ->
     NE_arrow
       (norm_expr ~term:true f e1,
        norm_expr ~term:true f e2),
     None
  | TE_pre e1 ->
     NE_pre (norm_expr ~term:true f e1), None
  | TE_op (op, el) ->
     begin
       match op with
       | Op_add | Op_sub | Op_mul | Op_div | Op_mod
       | Op_add_f | Op_sub_f | Op_mul_f | Op_div_f ->
          let e1, e2 = get_two_params el in
          NE_arith
            (op,
             norm_expr ~term:true f e1,
             norm_expr ~term:true f e2),
          None
       | Op_if ->
          begin
            match el with
              [ b ; e1 ; e2 ] ->
              NE_if
                (norm_expr ~term:false f b,
                 norm_expr ~term:true f e1,
                 norm_expr ~term:true f e2),
              None
            | _ -> assert false
          end
       | _ -> create_aux ty f e
     end
  | TE_tuple el ->
     NE_tuple (List.map (norm_expr ~term:true f) el), None
  | TE_app (id, el) ->
     let n_a = List.find (fun n -> (Ident.compare n.tn_name id) = 0) f in
     inlining el n_a f, None
  | TE_prim _ ->
     assert false

and norm_expr ~term f e : Norm_ast.n_expr =
  let desc, aux =
    if term then
      (norm_term_desc e.texpr_type f e.texpr_desc)
    else
      (norm_form_desc e.texpr_type f e.texpr_desc, None)
  in
  { nexpr_desc = desc;
    nexpr_type = e.texpr_type;
    nexpr_loc = e.texpr_loc;
    nexpr_aux = aux;
  }
     
and norm_equation f eq =
  let n_e = norm_expr ~term:true f eq.teq_expr in
  (* Format.printf "norm_equation@."; *)
  let p = eq.teq_patt in
  (* if the pattern is that of a tuple,
     we add equations for each element of the tuple,
     except the first one *)
  add_tuple
    (List.rev p.tpatt_desc)
    n_e
  ;
  (* Format.printf *)
  (*   "desc : %i, type : %i\n" *)
  (*   (List.length p.tpatt_desc) *)
  (*   (List.length p.tpatt_type) *)
  (* ; *)
  let patt =
    { npatt_desc = [List.hd p.tpatt_desc] ;
      npatt_type = [List.hd p.tpatt_type] ;
      npatt_loc = p.tpatt_loc ;
    }
  in
  { neq_patt = patt ; neq_expr = break_tuple 0 n_e }

    
let rec add_vars = function
  | [] -> ()
  | v::tl ->
     (* Format.printf "added to env : %s %s\n" (Ident.string_of (fst v)) (match (snd v) with *)
     (*   Tbool -> "bool" *)
     (* | Tint -> "int" *)
     (* | Treal -> "real") ; *)
    var_typ := Env.add (fst v) (snd v) !var_typ ;
    add_vars tl
    
let norm_node f n =
  (* Format.printf "norm_node %s\n" (Ident.string_of n.tn_name) ; *)
  let inputs = n.tn_input in
  let outputs = n.tn_output in
  let locals = n.tn_local in
  add_vars inputs ;
  add_vars outputs ;
  add_vars locals ;
  (* Format.printf "env fini@." ; *)
  (* print_env (Env.bindings !var_typ) ; *)
  let eqs = List.map (norm_equation f) n.tn_equs in
  (*  let new_eqs = add_eqs () in *)
  { nn_name = n.tn_name ;
    nn_input = inputs ;
    nn_output = outputs ;
    nn_local = locals@(!var_to_add) ;
    nn_equs = eqs@(!eq_to_add) ;
    nn_loc = n.tn_loc ;
  }
  
let norm_file f main =
  (* Format.printf "norm_file@."; *)
  [ norm_node f (List.find (fun n -> n.tn_name.Ident.name = main) f) ]
