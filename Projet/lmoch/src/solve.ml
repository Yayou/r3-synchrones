open Asttypes
open Norm_ast
open Format
open Aez
open Smt

module Env = Map.Make(String)

  
type error =
  | UnexpectedTuple

exception Error of location * error
let dummy_loc = Lexing.dummy_pos, Lexing.dummy_pos

let error loc e = raise (Error (loc, e))
(* let errors loc s = error loc (Other s) *)

let report fmt = function
  | UnexpectedTuple -> fprintf fmt "tuples should have disappeared after normalization"


(* the environment vars stores the bindings between
   the names of the variables and their declaration.
   var_out is the name of the variable to be checked *)
let vars = ref Env.empty
let var_out = ref ""
  (* the list forms stores the formulas *)
let forms = ref []

let num n = Term.make_int (Num.Int n)

(* we translate each type of constant to the appropriate formula *)
let const_aez = function
  | Cbool true  -> Term.t_true
  | Cbool false -> Term.t_false
  | Cint n      -> num n
(* | Creal n    -> Term.make_real (Num.Ratio n) *)
  | Creal _     -> assert false


let declare_symbol name t_in t_out =
  let x = Hstring.make name in
  Symbol.declare x t_in t_out ;
  x

let declare_symbol_int name t_out =
  declare_symbol name [ Type.type_int ] t_out

let base_ty_aez (t:Asttypes.base_ty) =
  match t with
  | Asttypes.Tbool -> Type.type_bool
  | Asttypes.Tint  -> Type.type_int
  | Asttypes.Treal -> Type.type_real
    
let ty_aez (t:Asttypes.ty) =
  match t with
  | [ ty ] -> base_ty_aez ty
  | _ -> assert false

let cmp_aez cmp t1 t2 =
  Formula.make_lit cmp [t1 ; t2]
     
let arith_aez op t1 t2 =
  Term.make_arith op t1 t2

let comb_aez cmb fl =
  Formula.make cmb fl
     
let rec create_aux aux n pre id e =
  Format.printf "aux %s" (Ident.string_of id) ;
  let a = Env.find (Ident.string_of id) !vars in
  let aux_n =
    Formula.make_lit Formula.Eq [ Term.make_app a [n] ; Term.t_true ] 
  in
  (* we compute the formula.
     If a formula is not a term (which is the case here),
     then its result will be a boolean, which means that
     we can use it in our auxiliary formula *)
  let f_aux n = form_aez aux n pre e in
  let f = fun n ->
    Formula.make Formula.And
		 [ Formula.make Formula.Imp [ aux_n ; f_aux n ] ;
		   Formula.make Formula.Imp [ f_aux n ; aux_n ] ]
  in
  aux := f :: !aux
        
and term_aez aux n pre e =
  match e.nexpr_desc with
  | NE_const c ->
     const_aez c
  | NE_ident id ->
     (* we translate a variable into a call to a function,
        taking into account the number of level of pre.
        By default, pre = 0, and it increases each time
        we are in a pre expression.
        That way, pre (x - 1) is translated to x(n-1)-1 *)
     begin
       begin
         match e.nexpr_aux with
           None -> ()
         | Some f -> create_aux aux n pre id f
       end ;
       try
         let var = Env.find (Ident.string_of id) !vars in
         if pre = 0 then
           Term.make_app var [n]
         else
           Term.make_app
             var
             [ Term.make_arith Term.Minus n (num pre) ]
       with
         Not_found -> assert false
     end
  | NE_if (b, e1, e2) ->
     Term.make_ite
       (form_aez aux n pre b )
       (term_aez aux n pre e1)
       (term_aez aux n pre e2)
  | NE_arrow (e1, e2) ->
     Term.make_ite
       (Formula.make_lit Formula.Eq [n ; (num 0)])
       (term_aez aux n pre e1)
       (term_aez aux n pre e2)
  | NE_pre e ->
     term_aez aux n (pre+1) e
  | NE_arith (op, e1, e2) ->
     let t1 = term_aez aux n pre e1 in
     let t2 = term_aez aux n pre e2 in
     begin
       match op with
       | Op_add | Op_add_f -> arith_aez Term.Plus  t1 t2
       | Op_sub | Op_sub_f -> arith_aez Term.Minus t1 t2
       | Op_mul | Op_mul_f -> arith_aez Term.Mult  t1 t2
       | Op_div | Op_div_f -> arith_aez Term.Div   t1 t2
       | Op_mod -> arith_aez Term.Modulo t1 t2
       | _ -> assert false
     end
  | _ ->
     Norm_ast_printer.print_exp std_formatter e ;
    assert false
       
and form_aez aux n pre e =
  match e.nexpr_desc with
  | NE_const (Cbool true) -> Formula.f_true
  | NE_const (Cbool false) -> Formula.f_false
  | NE_cmp (op, e1, e2) ->
     let t1 = term_aez aux n pre e1 in
     let t2 = term_aez aux n pre e2 in
     begin
       match op with
       | Op_eq  -> cmp_aez Formula.Eq t1 t2
       | Op_neq -> cmp_aez Formula.Neq t1 t2
       | Op_lt  -> cmp_aez Formula.Lt t1 t2
       | Op_le  -> cmp_aez Formula.Le t1 t2
       | _ -> assert false
     end
  | NE_comb (op, el) ->
     let fl = List.map (form_aez aux n pre) el in
     begin
       match op with
       | Op_and  -> comb_aez Formula.And fl
       | Op_or   -> comb_aez Formula.Or  fl
       | Op_impl -> comb_aez Formula.Imp fl
       | Op_not  -> comb_aez Formula.Not fl
       | _ -> assert false
     end
  | _ -> Formula.make_lit Formula.Eq [ term_aez aux n pre e ; Term.t_true ]


let eq_aez eq =
  (* we retrieve the variable for the pattern *)
  let p = eq.neq_patt in
  let id =
    match p.npatt_desc with
    | [ i ] -> (Ident.string_of i)
    | _     -> error p.npatt_loc UnexpectedTuple
  in
  let var =
    try
      Env.find id !vars
    with
      Not_found -> Format.printf "unbound var %s@." id ; assert false
  in
  (* we compute the formula corresponding to
     fun n -> var(n) = term_aez(eq) (n) *)
  let aux = ref [] in
  let t n = term_aez aux n 0 eq.neq_expr in
  let f_eq =
    fun n -> Formula.make_lit Formula.Eq [ Term.make_app var [n] ; t n ]
  in
  (* Format.printf "\nformule de l'equation avec %s@." id ; *)
  (* Formula.print Format.std_formatter (f_eq (num 1)) ; *)
  (* we add this equation to our list of formulas *)
  forms := f_eq :: (!aux @ !forms)


let rec add_vars = function
  | (id,t)::tl ->
     let n = Ident.string_of id in
     let var = declare_symbol_int n (base_ty_aez t) in
     vars := Env.add n var !vars ;
     add_vars tl
  | [] -> ()

let node_aez n main =
  if n.nn_name.Ident.name = main then
    (match n.nn_output with
      [(id,_)] -> var_out := Ident.string_of id
    | _ -> assert false)
  ;
  add_vars n.nn_input ;
  add_vars n.nn_output ;
  add_vars n.nn_local ;
  List.iter eq_aez n.nn_equs
    

let rec k_ind_aux delta_incr p_incr k n =
  if k = 10 then
    true, false
  else
    let module BMC_solver = Smt.Make(struct end) in
    let module IND_solver = Smt.Make(struct end) in
    if k = 0 then (
      BMC_solver.assume ~id:0 (delta_incr (num 0)) ;
      BMC_solver.check() ;
      let base = BMC_solver.entails ~id:0
                                    (p_incr (num 0))
      in
      IND_solver.assume ~id:0 (Formula.make_lit Formula.Le [(num 0) ; n]) ;
      let n_plus_one = Term.make_arith Term.Plus n (num 1) in
      IND_solver.assume ~id:0 (delta_incr n) ;
      IND_solver.assume ~id:0 (p_incr n) ;
      IND_solver.assume ~id:0 (delta_incr n_plus_one) ;
      IND_solver.check() ;
      let ind = IND_solver.entails ~id:0 (p_incr (Term.make_arith Term.Plus n (num 1)))
      in
      (if (base = true && ind = false) then
        k_ind_aux delta_incr p_incr 1 n
      else
        base, ind)
    )
    else
      let p_incr_b = ref [] in
      for i = 0 to k do
        BMC_solver.assume ~id:0 (delta_incr (num i)) ;
        p_incr_b := (p_incr (num i)) :: !p_incr_b ;
      done ;
      BMC_solver.check() ;
      let base = BMC_solver.entails ~id:0
                                    (Formula.make Formula.And (List.rev !p_incr_b))
      in
      IND_solver.assume ~id:0 (Formula.make_lit Formula.Le [(num 0) ; n]) ;
      for i = 0 to k+1 do
        let n_plus_i = Term.make_arith Term.Plus n (num i) in
        IND_solver.assume ~id:0 (delta_incr n_plus_i) ;
        if i != k+1 then
          IND_solver.assume ~id:0 (p_incr n_plus_i)
      done ;
      IND_solver.check() ;
      let ind = IND_solver.entails ~id:0 (p_incr (Term.make_arith Term.Plus n (num (k+1))))
      in
      if (base = true && ind = false) then
        k_ind_aux delta_incr p_incr (k+1) n
      else
        base, ind
                                      
            
let k_induction delta_incr p_incr =
  let n = Term.make_app (declare_symbol "n" [] Type.type_int) [] in    
  k_ind_aux delta_incr p_incr 0 n

            
let file_aez f main =
  begin
    match f with
      [ n ] -> node_aez n main
    | _ -> assert false
  end ;
  (* at this point, forms contains a list of the formulas
     which describe the main node, vars the environment of
     variables, and var_out the name of the variable returned,
     and all variables have been declares as symbols *)
  (* we now define the definition and the invariant to prove : *)
  let form_app n = List.map (fun f -> f n) !forms in
  let delta_incr n = Formula.make Formula.And (form_app n) in
  let out = Env.find !var_out !vars in
  let p_incr n =
    Formula.make_lit Formula.Eq [ Term.make_app out [n]; Term.t_true ]
  in
  Format.printf "\n/* Descripteur : */@." ;
  Formula.print Format.std_formatter (delta_incr (num 1)) ;
  Format.printf "\n/* Formule a verifier : */@." ;
  Formula.print Format.std_formatter (p_incr (num 0)) ;
  Format.printf "@." ;
  k_induction delta_incr p_incr
(* (/* we begin by trying to prove using basic induction : */)
  let module BMC_solver = Smt.Make(struct end) in
  let module IND_solver = Smt.Make(struct end) in
  let base =
    BMC_solver.assume ~id:0 (delta_incr (num 0)) ;
    BMC_solver.assume ~id:0 (delta_incr (num 1)) ;
    BMC_solver.check() ;
    BMC_solver.entails ~id:0
      (Formula.make Formula.And [ p_incr (num 0) ;
                                  p_incr (num 1)])
  in
  let ind =
    let n = Term.make_app (declare_symbol "n" [] Type.type_int) [] in
    let n_plus_one = Term.make_arith Term.Plus n (num 1) in
    IND_solver.assume ~id:0 (Formula.make_lit Formula.Le [(num 0) ; n]) ;
    IND_solver.assume ~id:0 (delta_incr n) ;
    IND_solver.assume ~id:0 (delta_incr n_plus_one) ;
    IND_solver.assume ~id:0 (p_incr n) ;
    IND_solver.check() ;
    IND_solver.entails ~id:0 (p_incr n_plus_one)
  in
  if not base then Format.printf "FALSE PROPERTY"
  else if ind then Format.printf "TRUE PROPERTY"
  else Format.printf "Don't know" ;
  base, ind
*)
