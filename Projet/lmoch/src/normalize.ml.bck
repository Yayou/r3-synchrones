open Asttypes
open Norm_ast
open Typed_ast

module Env = Map.Make(Ident)

(* we create a counter for the auxiliary equations
   and a list of tuples (id, expr) of equations id = expr to be added *)
let cpt_aux = ref 0
let eq_to_add = ref []
let var_to_add = ref []

(* we also create a global environnement to match
   variables with their type.
   We have to do so to create correct patterns when adding equations *)
let var_typ = ref Env.empty

let will_be_term e =
  match e.texpr_desc with
  | TE_const _ | TE_ident _ | TE_arrow _ | TE_pre _ | TE_tuple _ -> true
  | TE_op (op,_) ->
     begin
       match op with
       | Op_and | Op_or | Op_impl | Op_not
       | Op_eq | Op_neq | Op_lt | Op_le | Op_gt | Op_ge
         -> false
       | _ -> true
     end
  | _ -> false
     
(* let rec add_aux nt e = *)
(*   if not(will_be_term e) then *)
(*     let id = Ident.make ("aux"^(string_of_int !cpt_aux)) Ident.Stream in *)
(*     incr cpt_aux ; *)
(*     eq_to_add := (id, norm_expr nt e)::!eq_to_add ; *)
(*     var_typ := Env.add id (List.hd e.texpr_type) !var_typ ; *)
(*     { e with texpr_desc = TE_ident id } *)
(*   else *)
(*     norm_expr nt e *)

let rec add_aux nt e  =
  if not(will_be_term e) then
    let id = Ident.make ("aux"^(string_of_int !cpt_aux)) Ident.Stream in
    incr cpt_aux ;
    let f = norm_expr nt e in
    var_to_add := (id, List.nth e.texpr_type nt) :: !var_to_add ;
    { nexpr_desc = NE_ident id;
      nexpr_type = e.texpr_type;
      nexpr_loc = e.texpr_loc;
      nexpr_aux = Some f;
    }
  else
    norm_expr nt e
           
(* depending on what the operation will be translated as,
   we add an aux equation to the expressions *)
and norm_binop_t nt o el = match o with
  (* f comb f *)
  | Op_and | Op_or | Op_impl | Op_not ->
     let nl = List.map (norm_expr nt) el in
     NE_op (o, nl), None
  (* t cmp t *)
  | Op_eq | Op_neq | Op_lt | Op_le ->
     (* modification for aux *)
     let nl = List.map (add_aux nt) el in
     NE_op (o, nl), None
  (* in aez, there is only < and <=, not > nor >=,
     so we transform > in < by reversing the order of the parameters *)
  | Op_gt ->
     (* modification for aux *)
     let al = List.rev_map (add_aux nt) el in
     NE_op (Op_lt, al), None
  | Op_ge ->
     (* modification for aux *)
     let al = List.rev_map (add_aux nt) el in
     NE_op (Op_le, al), None
  (* t op t *)
  | Op_add | Op_sub | Op_mul | Op_div | Op_mod
  | Op_add_f | Op_sub_f | Op_mul_f | Op_div_f ->
     (* modification for aux *)
     let nl = List.map (add_aux nt) el in
     NE_op (o, nl), None
  (* ite(f,t,t) *)
  | Op_if ->
     begin
       match el with
       | [ b ; e1 ; e2 ] ->
          (* modif pr aux *)
          let ne1 = (norm_expr nt) e1 in
          let ne2 = (norm_expr nt) e2 in
          NE_op (o, [ b ; ne1 ; ne2 ]), None
       | _ -> assert false
     end

and norm_term num_tuple e =
  (* Format.printf "norm_expr\n"; *)
  let desc, aux = norm_term_desc num_tuple e.texpr_desc in
  { nexpr_desc = desc;
    nexpr_type = e.texpr_type;
    nexpr_loc = e.texpr_loc;
    nexpr_aux = aux;
  }
    
and norm_term_desc num_tuple e =
  match e with
  | TE_const c ->
     NE_const c, None
  | TE_ident i ->
     NE_ident i, None
  | TE_arrow (e1, e2) ->
     NE_arrow (norm_term num_tuple e1) (norm_term num_tuple e2),
     None
     (* let ne1 = add_aux num_tuple e1 in *)
     (* let ne2 = add_aux num_tuple e2 in *)
  (* NE_arrow (ne1, ne2), None *)
  | TE_pre e ->
     NE_pre (norm_term num_tuple e), None
  | TE_op (o, el) ->
     begin
       match op with
       | Op_add | Op_sub | Op_mul | Op_div | Op_mod
       | Op_add_f | Op_sub_f | Op_mul_f | Op_div_f ->
                                           norm_binop_t num_tuple o el
       | Op_if ->
          begin
            match el with
              [ b ; e1 ; e2 ] ->
              NE_if
                (norm_form num_tuple b)
                (norm_term num_tuple e1)
                (norm_term num_tuple e2),
              None
            | _ -> assert false
          end
       | _ -> create_aux num_tuple e
     norm_op num_tuple o el     
  | TE_tuple el ->
     norm_term_desc
       (* typing forbids nested tuples, so the value of
          num_tuple is no longer useful *)
       num_tuple
       (List.nth el num_tuple).texpr_desc
  | TE_app _ ->
     assert false
  | TE_prim _ ->
     assert false


let create_patt i =
  (* Format.printf "create_patt\n"; *)
  let t = Env.find i !var_typ in
  (* Format.printf *)
  (*   "create_patt %s %s\n" *)
  (*   (Ident.string_of i) *)
  (*   (match t with | Tbool -> "bool" | Treal -> "real" | Tint -> "int"); *)
  { tpatt_desc = [i] ;
    tpatt_type = [t] ;
    tpatt_loc = (Lexing.dummy_pos,Lexing.dummy_pos) ;
  }
      
let create_eq i e =
  (* Format.printf "create_eq %s\n" (Ident.string_of i); *)
  { teq_patt = create_patt i ;
    teq_expr = e ;
  } 
      
let rec add_eqs () =
  (* Format.printf "add_eqs\n"; *)
  match !eq_to_add with
  | [] -> []
  | (id,expr)::tl ->
     let eq = create_eq id expr in
     eq_to_add := tl ;
     eq :: (add_eqs ())

let rec add_tuple ids e =
  match ids with
  | [] | [_] -> ()
  | hd::tl ->
     eq_to_add := (hd, norm_expr (List.length tl) e) :: !eq_to_add;
    add_tuple tl e
     
let norm_equation eq =
  (* Format.printf "norm_equation\n"; *)
  let p = eq.teq_patt in
  (* if the pattern is that of a tuple,
     we add equations for each element of the tuple,
     except the first one *)
  add_tuple
    (List.rev p.tpatt_desc)
    eq.teq_expr
  ;
  (* Format.printf *)
  (*   "desc : %i, type : %i\n" *)
  (*   (List.length p.tpatt_desc) *)
  (*   (List.length p.tpatt_type) *)
  (* ; *)
  let patt =
    { tpatt_desc = [List.hd p.tpatt_desc] ;
      tpatt_type = [List.hd p.tpatt_type] ;
      tpatt_loc = p.tpatt_loc ;
    }
  in
  let expr = norm_expr 0 eq.teq_expr in
  { teq_patt = patt ; teq_expr = expr }

(* let rec print_env = function *)
(*   | [] -> () *)
(*   | (id,ty)::tl -> *)
(*      let p_id = Ident.string_of id in *)
(*      let p_ty = begin *)
(*        match ty with *)
(*          Tbool -> "bool" *)
(*        | Tint -> "int" *)
(*        | Treal -> "real" *)
(*      end in *)
(*      Format.printf "%s %s\n" p_id p_ty ; *)
(*      print_env tl *)

    
let rec add_vars = function
  | [] -> ()
  | v::tl ->
     (* Format.printf "added to env : %s %s\n" (Ident.string_of (fst v)) (match (snd v) with *)
     (*   Tbool -> "bool" *)
     (* | Tint -> "int" *)
     (* | Treal -> "real") ; *)
    var_typ := Env.add (fst v) (snd v) !var_typ ;
    add_vars tl
    
let norm_node n =
  (* Format.printf "norm_node %s\n" (Ident.string_of n.tn_name) ; *)
  add_vars n.tn_input ;
  add_vars n.tn_output ;
  add_vars n.tn_local ;
  (* Format.printf "env fini\n" ; *)
  (* print_env (Env.bindings !var_typ) ; *)
  let eqs = List.map (norm_equation) n.tn_equs in
  let new_eqs = add_eqs () in
  { n with tn_equs = (eqs@new_eqs) }
  
let norm_file f =
  (* Format.printf "norm_file\n"; *)
  List.map norm_node f
