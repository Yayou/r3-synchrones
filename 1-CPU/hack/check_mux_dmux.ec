node check_mux_dmux
  (in: bool;
  sel: bool)
returns
  (ok: bool);

let
  ok = (in = (if sel then (if sel then in else false) else (if sel then false 
  else in)));
tel

