node check_add
  (in: bool)
returns
  (ok: bool);

var
  V340_c_0: bool;
  V341_c_1: bool;
  V342_c_2: bool;
  V343_c_3: bool;
  V508_c0: bool;
  V509_s1: bool;
  V510_c1: bool;
  V511_c2: bool;
  V512_c0: bool;
  V513_s1: bool;
  V514_c1: bool;
  V515_c2: bool;
  V516_c0: bool;
  V517_s1: bool;
  V518_c1: bool;
  V519_c2: bool;
  V520_c0: bool;
  V521_s1: bool;
  V522_c1: bool;
  V523_c2: bool;

let
  ok = ((((V340_c_0 = true) and (V341_c_1 = true)) and (V342_c_2 = true)) and (
  V343_c_3 = false));
  V340_c_0 = (false xor V509_s1);
  V341_c_1 = (V508_c0 xor V513_s1);
  V342_c_2 = (V512_c0 xor V517_s1);
  V343_c_3 = (V516_c0 xor V521_s1);
  V508_c0 = (V510_c1 or V511_c2);
  V509_s1 = (true xor true);
  V510_c1 = (true and true);
  V511_c2 = (false and V509_s1);
  V512_c0 = (V514_c1 or V515_c2);
  V513_s1 = (true xor true);
  V514_c1 = (true and true);
  V515_c2 = (V508_c0 and V513_s1);
  V516_c0 = (V518_c1 or V519_c2);
  V517_s1 = (true xor true);
  V518_c1 = (true and true);
  V519_c2 = (V512_c0 and V517_s1);
  V520_c0 = (V522_c1 or V523_c2);
  V521_s1 = (true xor true);
  V522_c1 = (true and true);
  V523_c2 = (V516_c0 and V521_s1);
tel

